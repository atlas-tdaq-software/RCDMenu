#ifndef RCDMENU_H
#define RCDMENU_H

//******************************************************************************
// file: RCDMenu.h
// desc: library for text-based menu (for testing purposes only!)
// auth: 26/04/00 R. Spiwoks
// modf: 11/28/02 R. Spiwoks, DataFlow repository
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
// modf: 02-FEB-2009 R. Spiwoks - mods necessary for gcc43 + clean-up
//******************************************************************************
//
// $Ids$

#include <string>
#include <vector>
#include <setjmp.h>
#include <cstdlib>
#include <cerrno>
#include <algorithm>

using namespace std;

namespace RCD {

//------------------------------------------------------------------------------

class MenuItem {
  public:
    MenuItem();
    virtual ~MenuItem();

    inline const std::string getName() const            { return(name); }
    inline void  setName(const char * n)                { name = n; }
    inline int   getType() const                        { return(type); }
    inline void  setType(const int t)                   { type = t; }

    virtual int  action()                               { return(0); }

  protected:
    int           enterInt(const char*, int, int);
    unsigned long enterHex(const char*, unsigned long, unsigned long);
    unsigned long enterNumber(const char*, unsigned long, unsigned long);
    std::string   enterString(const char*);

    std::string                         name;
    int                                 type;   // object type (RTTI?)

};     // class MenuItem

//------------------------------------------------------------------------------

class Menu : public MenuItem {
  public:
    Menu(const char* n = 0);
   ~Menu();

    inline const std::string getTitle() const           { return(title); }
    inline void         setTitle(const std::string& t)  { title = t; }

    int  add(MenuItem* item);
    inline void init(MenuItem* item)                    { init_item = item; }
    inline void exit(MenuItem* item)                    { exit_item = item; }
    int  execute(void);
    inline int  action()                                { return(execute()); }
    void draw();

  private:
    int installHandler();
    static void handler(int);

    std::string                         title;
    vector<MenuItem*>*                  item_list;
    MenuItem*                           init_item;
    MenuItem*                           exit_item;
    static int                          menu_user;
    static int                          menu_flag;
    static sigjmp_buf                   menu_jump;

};     // class Menu

}      // namespace RCD

#endif // RCDMENU_H
