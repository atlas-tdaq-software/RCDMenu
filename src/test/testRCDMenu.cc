//******************************************************************************
// file: testRCDMenu.cc
// desc: test of library for text-based menu; also example file
// auth: 26/04/00 R. Spiwoks
// modf: 28/11/02 R. Spiwoks, DataFlow repository
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
//******************************************************************************

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <unistd.h>

#include "DFDebug/DFDebug.h"
#include "RCDMenu/RCDMenu.h"

using namespace RCD;

//------------------------------------------------------------------------------

class MenuItemExampleA : public MenuItem {
  public:
    MenuItemExampleA() { setName("MenuItemExampleA"); }
    int action() {
	std::cout << "This is the derived action for MenuItemExampleA ..." << std::endl;
	int i = enterInt("integer value",0,100000);
	std::cout << "integer value = " << i << std::endl;
	sleep(10);
	if(i>0) {
	    return(-1);
	}
	return(0);
    }
};

//------------------------------------------------------------------------------

class MenuItemExampleB : public MenuItem {
  public:
    MenuItemExampleB() { setName("MenuItemExampleB"); }
    int  action() {
	std::cout << "This is the derived action for MenuItemExampleB ..." << std::endl;
	unsigned long i = enterHex("hexadecimal value",0,0xffffffff);
	std::printf("hexadecimal value = %08lx  (decimal = %ld)\n",i,i);
	return(0);
    }
};

//------------------------------------------------------------------------------

class MenuItemExampleC : public MenuItem {
  public:
    MenuItemExampleC() { setName("MenuItemExampleC"); }
    int  action() {
	std::cout << "This is the derived action for MenuItemExampleC ..." << std::endl;
	std::string str = enterString("string");
	std::printf("string value = \"%s\"\n",str.c_str());
	return(0);
    }
};

//------------------------------------------------------------------------------

int main() {

    Menu	menu("mainMenu");
    Menu	menuA("subMenuA");
    Menu	menuB("subMenuB");
    Menu	menuC("subsubMenuC");

    // Define debug settings
    DF::GlobalDebugSettings::setup(20,DFDB_RCDMENU);

    menu.add(new MenuItemExampleA);
    menu.add(new MenuItemExampleB);
    menu.add(new MenuItemExampleC);
    menu.add(&menuA);
    menu.add(&menuB);

    menuA.init(new MenuItemExampleC);
    menuA.add(new MenuItemExampleA);
    menuA.add(new MenuItemExampleA);
    menuA.exit(new MenuItemExampleC);

    menuB.add(new MenuItemExampleB);
    menuB.add(new MenuItemExampleB);
    menuB.add(&menuC);

    menuC.add(new MenuItemExampleC);

    menu.execute();
}
