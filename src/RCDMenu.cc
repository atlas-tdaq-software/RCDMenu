//******************************************************************************
// file: RCDMenu.cc
// desc: library for text-based menu (for testing purposes only!)
// auth: 26/04/00 R. Spiwoks
// modf: 28/11/02 R. Spiwoks, DataFlow repository
// modf: 16/01/04 M. Gruwe: Use DFDebug instead of RCDUtilities
// modf: 23/01/04 M. Gruwe: Fix bug (specify std:: when necessary)
// modf: 02-FEB-2009 R. Spiwoks - mods necessary for gcc43 + clean-up
//******************************************************************************
//
// $Id$

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <signal.h>
#include <stdlib.h>

#include "DFDebug/DFDebug.h"
#include "RCDMenu/RCDMenu.h"

using namespace RCD;

//------------------------------------------------------------------------------

MenuItem::MenuItem() {

    // set default name (=quit)
    setName("quit");

    // set object type (0=MenuItem)
    setType(0);
}

//------------------------------------------------------------------------------

MenuItem::~MenuItem() {

}

//------------------------------------------------------------------------------

int MenuItem::enterInt(const char* prompt, int lo, int hi) {

    int val;
    char buf[20];

    while(1) {
        std::printf("%s [%d..%d]: ",prompt,lo,hi);
        buf[0] = '\0';
        if(!cin) cin.clear();
        cin >> buf;
        if(buf[0] == '\0') {
            continue;
        }
        else {
            val = strtol(buf,(char **)0,0);
            if((val < lo) || (val > hi)) {
                continue;
            }
            else {
                break;
            }
        }
    }
    // clean up left-over newline character
    cin.getline(buf,1);

    return(val);
}

//------------------------------------------------------------------------------

unsigned long MenuItem::enterHex(const char* prompt, unsigned long lo, unsigned long hi) {

    unsigned long val;
    char buf[20];

    while(1) {
        std::printf("%s [%08lx..%08lx]: ",prompt,lo,hi);
        buf[0] = '\0';
        if(!cin) cin.clear();
        cin >> buf;
        if(buf[0] == '\0') {
            continue;
        }
        else {
            // kludge for value "ffffffff"
            val = (unsigned long)strtoul(buf, (char **)0, 16);
            if((val < lo) || (val > hi)) {
                continue;
            }
            else {
                break;
            }
        }
    }
    // clean up left-over newline character
    cin.getline(buf,1);

    return(val);
}

//------------------------------------------------------------------------------

unsigned long MenuItem::enterNumber(const char* prompt, unsigned long lo, unsigned long hi) {

    std::string s;
    unsigned long val;
    char buf[64];
    char* end;
    int base(10);

    while(1) {
        std::printf("%s [0x%08lx(= %ld)..0x%08lx(= %ld)]: ",prompt,lo,lo,hi,hi);
        buf[0] = '\0';
        if(!cin) cin.clear();
        cin >> buf;
        if(buf[0] != '\0') {

            // copy string
            s = buf;

            // remove space(s)
            s.erase(remove(s.begin(),s.end(),' '),s.end());

            // check if string empty
            if(s.empty()) {
                std::printf("ERROR: STRING EMPTY!!!!\n\n");
                continue;
            }

            // check if string starts with minus sign
            if(s[0] == '-') {
                std::printf("ERROR: NEGATIVE VALUE!!!!\n\n");
                continue;
            }

            // check if string is hexadecimal, binary, or octal
            if((s.size() > 2) && ((s.substr(0,2) == "0x") || s.substr(0,2) == "0X")) {
                base = 16;
            }
            else if((s.size() > 2) && ((s.substr(0,2) == "0b") || s.substr(0,2) == "0B")) {
                s.erase(0,2);
                base = 2;
            }
            else if((s.size() > 1) && ((s.substr(0,1) == "0"))) {
               base = 8;
            }

            // try to convert string to integer
            errno = 0;
            val = std::strtoul(s.c_str(),&end,base);
            if(end[0] != '\0') {
                std::printf("\nERROR: INVALID CHARACTER!!!!\n\n");
                continue;
            }
            else if(errno == ERANGE) {
                std::printf("\nERROR: OUT OF RANGE!!!!\n\n");
                continue;
            }
            else if(errno == EINVAL) {
                std::printf("\nERROR: INVALID CONVERSION!!!!\n\n");
                continue;
            }

            if((val < lo) || (val > hi)) {
                std::printf("ERROR: OUT OF RANGE!!!!\n\n");
                continue;
            }
            else {
                break;
            }
        }
    }
    // clean up left-over newline character
    cin.getline(buf,1);

    return(val);
}

//------------------------------------------------------------------------------

std::string MenuItem::enterString(const char* prompt) {

    std::string val;
    char buf[256];

    std::printf("%s: ",prompt);
    cin.getline(buf,256);
    val = buf;

    return(val);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

int             Menu::menu_user = 0;
int             Menu::menu_flag = 0;
sigjmp_buf      Menu::menu_jump;

//------------------------------------------------------------------------------

#if DEBUG_LEVEL>0
void Menu::handler(int sig) {
#else
void Menu::handler(int) {
#endif /* DEBUG_LEVEL */

    DEBUG_TEXT(DFDB_RCDMENU,20,"return from interrupt with signal " << std::dec << sig);

    // jump to last executed menu
    menu_flag++;
    siglongjmp(menu_jump,1);
}

//------------------------------------------------------------------------------

Menu::Menu(const char* n) : init_item(0), exit_item(0) {

    // set menu name
    if(n == 0)
       setName("menu");
    else
       setName(n);

    // set object type (1=Menu)
     setType(1);

    // set menu title
    setTitle(name);

    // get new item list
    item_list = new vector<MenuItem*>;

    // add default item (=quit)
    add(new MenuItem);

    // increment number of users and install signal handler
    menu_user++;
    installHandler();
}

//------------------------------------------------------------------------------

Menu::~Menu(void) {

    delete item_list;
}

//------------------------------------------------------------------------------

int Menu::add(MenuItem* item) {

    std::string ntit;
    Menu* menu;

    DEBUG_TEXT(DFDB_RCDMENU,20,"item = " << item->getName().c_str() << ", type = " << item->getType());
    //*** add item to list of items
    item_list->push_back(item);

    //*** if item of type menu then change the title
    if(item->getType() == 1) {
        ntit = std::string(title) + std::string("/") + std::string(item->getName());
        DEBUG_TEXT(DFDB_RCDMENU,20,"title = " << ntit.c_str());
        menu = (Menu*) item;
        menu->setTitle(ntit);
    }

    return(0);
}

//------------------------------------------------------------------------------

int Menu::execute(void) {

    int m,r;

    // excute init item
    if(init_item != 0) {
        r = init_item->action();
        if(init_item->getName() != std::string("quit")) {
            std::printf("\n  \"%s\" returns %d\n",init_item->getName().c_str(),r);
        }
    }

    // up-date context for jump statements
    menu_flag++;

    // draw menu and poll on input
    m = 1;
    while(m) {

        // set return from signal handler
        while(menu_flag) {
            menu_flag--;
            sigsetjmp(menu_jump,1);
        }

        draw();
        m = enterInt("\n  Enter number",0,item_list->size()-1);
        r = ((*item_list)[m])->action();
        if(((*item_list)[m])->getName() != std::string("quit")) {
            std::printf("\n  \"%s\" returns %d\n",((*item_list)[m])->getName().c_str(),r);
        }
    }

    // excute exit item
    if(exit_item) {
        r = exit_item->action();
        if(exit_item->getName() != std::string("quit")) {
            std::printf("\n  \"%s\" returns %d\n",exit_item->getName().c_str(),r);
        }
    }

    // up-date context for jump statements
    menu_flag++;

    return(r);
}

//------------------------------------------------------------------------------

void Menu::draw(void) {

    int in = 0;

    std::cout << std::endl << ">>>>>> " << title << " <<<<<<\n" << std::endl;
    vector<MenuItem*>::iterator it = item_list->begin();
    while(it != item_list->end()) {
        std::printf("    %2d  ",in++); std::cout << (*it++)->getName() << std::endl;
    }
}

//------------------------------------------------------------------------------

int Menu::installHandler(void) {

    struct sigaction sa;

    if(menu_user == 1) {
        // install signal handler: catch SIGINT
        sa.sa_handler = (void (*)(int))Menu::handler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        if(sigaction(SIGINT,&sa,(struct sigaction *)0)) {
            ERR_TEXT(DFDB_RCDMENU,"installing signal handler");
            return(-1);
        }
        DEBUG_TEXT(DFDB_RCDMENU,15,"signal handler installed for SIGINT");
    }
    return(0);
}
